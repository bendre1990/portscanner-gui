﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using NetTools;
using System.Net.NetworkInformation;


namespace PortScanner
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            // Set Default values for the Inputs
            InitializeComponent();
            fromip.Text = "192.168.2.100";
            toip.Text = "192.168.2.107";
            portbox.Text = "21,22,80,443,445,3306";
        }

        // Button Click Method
        private void ScanButton_Click(object sender, RoutedEventArgs e)
        {
            // Clear the TreeView so we can fill it with the new results
            DataTree.Items.Clear();

            // Declaring some variables ...
            var hosts_to_scan = IPAddressRange.Parse(fromip.Text + "-" + toip.Text);
            var ports_to_scan = new List<int>();

            // Here we parse the comma separated port input into a list
            string[] portvals = portbox.Text.Split(',');
            var portvals1 = portvals.Select(int.Parse).ToList();

            // Add it the to another list (whhyyyy????)
            foreach (int item in portvals1)
            {
                ports_to_scan.Add(item);
            }

            // Get Info for Statusbar
            int portcount = ports_to_scan.Count();
            int hostcount = 0;
            int totalendpoints = 0;
            foreach (var host1 in hosts_to_scan)
            {
                hostcount++;
            }
            totalendpoints = hostcount * portcount;
            StatusBarText.Text = string.Format("Start scanning {0} Endpoints", totalendpoints.ToString());

            // Start of scan (for each host to scan)
            foreach (var host1 in hosts_to_scan)
            {
                //  Generate a new TreeViewItem
                TreeViewItem treeItem = null;
                treeItem = new TreeViewItem();
                treeItem.Header = host1;

                // Add it to the Parent Tree
                if (PingTest(host1.ToString()) == true)
                {
                    DataTree.Items.Add(treeItem);
                    // Start a Thread for each Port to Scan
                    foreach (var port1 in ports_to_scan)
                    {
                        ThreadStart childref = delegate { Attempt(host1.ToString(), port1, treeItem); };
                        Thread childThread = new Thread(childref);
                        childThread.Start();

                    }
                }
                
                
            }
        }

        public static bool PingTest(string host)
        {
            bool pingable = false;
            Ping pinger = null;
            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(host);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }

            return pingable;
        }

        private void Attempt(string host, int port, TreeViewItem treeItem)
        {
            // Declaring Socket
            Socket sock = new Socket(AddressFamily.InterNetwork,
            SocketType.Stream,
            ProtocolType.Tcp);

            // Beginning an async connection (does it need to be async, since we run it in a new thread?)
            IAsyncResult result = sock.BeginConnect(host, port, null, null);

            // Waiting .. max 1 sek .. or something like that?
            result.AsyncWaitHandle.WaitOne(100, true);

            // If we connect (works instantly, does it skip the line above??)
            if (sock.Connected)
            {
                // If Success, end the connection
                Console.WriteLine(string.Format("Success! - ENDPOINT: {0}:{1}", host, port));
                sock.EndConnect(result);
                // Invoking the command to the main thread, to actually insert the child item to the parent item inside the treeview widget
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    treeItem.Items.Add(new TreeViewItem() { Header = port });
                }));

            }
            else
            {
                // If Fail, just close and forget (THis somehow does wait for the Async wait handle before the if statement)
                sock.Close();
                sock.Dispose();
            }
        }
    }
}
